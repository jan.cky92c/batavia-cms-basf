<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Users;
use App\Models\Privileges;
use Illuminate\Support\Facades\Hash;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    protected $imageProfile = '/imageFile/imageProfile/';

    public function viewProfile(){
        $profile = Users::where('id_employee', session('nik'))->first();
        if($profile->id_privilege){
            $getPriviledUser = Privileges::where('id', $profile->id_privilege)->first();
            $profile->privilege = $getPriviledUser->name;
        }
        if ($profile) {
            return view('profile.view_profile')->with('profile', $profile);
        }
    }

    public function editProfile(){
        $profile = Users::where('id_employee', session('nik'))->first();
        $getAllPrivileges = Privileges::get();
        if ($profile) {
            return view('profile.edit_profile', ['profile'=>$profile, 'dataPrivileges'=>$getAllPrivileges]);
        }
    }

    public function saveUpdateProfile(Request $request){
        $nik = session('nik');
        $profile = Users::where('id_employee', session('nik'))->first();
        $profile->name = request('name');
        $profile->email = request('email');
        $profile->id_privilege = request('privilege');
        $profile->position = request('position');
        $profile->gender = request('gender');
        $profile->is_active = request('status');

        $profile->phone = request('phone');
        $profile->birth_date = request('birth_date');
        if (request()->hasFile('image')) {
            if (request()->file('image')->isValid()) {

                $file_ext        = request()->file('image')->getClientOriginalExtension();
                $file_size       = request()->file('image')->getClientSize();
                $allow_file_exts = array('jpeg', 'jpg', 'png');
                $max_file_size   = 1024 * 1024 * 10;
                if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                    $dest_path     = public_path() . $this->imageProfile;
                    $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', request()->file('image')->getClientOriginalName());
                    $file_name     = str_replace(' ', '-', $file_name);
                    $imageProfileName = "image-profile-".$profile->name. '.' . $file_ext;
                    // move file to serve directory
                    request()->file('image')->move($dest_path, $imageProfileName);
                    $profile->image = $imageProfileName;
                }
            }
        }
        $request->session()->put('image', $profile->image);
        $profile->save();
        return redirect('profile')->with('message', 'Berhasil Diubah!');
    }

    public function saveUpdatePassword(){
        // $nik = session('nik');
        $profile = Users::where('id_employee', session('nik'))->first();
        if (Hash::check(request('oldpass'), $profile['password'])) {
            if(request('newPassword') == request('confirmNewPassword')){
                $profile['password'] = Hash::make(request('newPassword'));
                $profile->save();
                return view('profile.view_profile')->with('profile', $profile)->with('message', 'Berhasil Diubah');    
            } else {
                return view('profile.change_password')->with('profile', $profile)->with('message', 'Password Lama Tidak Sesuai');
            }
        } else {
            return view('profile.change_password')->with('profile', $profile)->with('message', 'Password Lama Tidak Sesuai');
        }
    }
}
