<?php

namespace App\Http\Controllers;

use DB;
use App\Models\CheckOutSections;
use Illuminate\Http\Request;

class CheckoutSectionController extends Controller
{
    public function sectionlist(){
        $section = CheckOutSections::all();
        if ($section) {
            return view('check_out_section.section_list')->with('datasection', $section);
        }
    }
    public function create(){
        $section = new CheckOutSections;
        $section->description = request('desc');
        $section->save();
        return redirect('/checkout-section')->with('message', 'Berhasil Ditambahkan!');
    }

    public function edit(Request $request, $section_id){
        $section = CheckOutSections::find($section_id);
        if ($section){
            return view('check_out_section.edit_section')->with('section', $section);
        }
    }
    public function delete(Request $request, $section_id){
        $section = CheckOutSections::find($section_id);
        $section->delete();
        return redirect('/checkout-section')->with('message', 'Berhasil Dihapus!');
    }

    public function saveUpdate(){
        $section = CheckOutSections::find(request('id'));
        $section->description = request('desc');
        $section->save();
        return redirect('/checkout-section')->with('message', 'Berhasil Ditambahkan!');
    }
}
