<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Transportertypes;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class TransporterController extends Controller
{
    public function daftartransporter(){
        $transporter = Transportertypes::all();
        if ($transporter) {
            return view('transporter_type.type_list')->with('datatransporter', $transporter);
        }
    }

    public function edit(Request $request, $transporter){
        $transporter = Transportertypes::find($transporter);
        if ($transporter){
            return view('transporter_type.edit_type')->with('transporter', $transporter);
        }
    }

    public function saveUpdate(){
        $id = request('id');
        $transporter = Transportertypes::find($id);
        $transporter->type = request('type');
        $transporter->description = request('desc');
        $transporter->save();
        return redirect('/transporter-type')->with('message', 'Berhasil Diubah!');
    }

    public function saveCreate(){
        $transporter = new Transportertypes;
        $transporter->type = request('type');
        $transporter->description = request('desc');
        $transporter->save();
        return redirect('/transporter-type')->with('message', 'Berhasil Ditambahkan!');
    }
    public function delete(Request $request, $transporter_id){
        $transporter = Transportertypes::find($transporter_id);
        $transporter->delete();
        return redirect('/transporter-type')->with('message', 'Berhasil Dihapus!');
    }
}
