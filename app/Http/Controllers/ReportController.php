<?php

namespace App\Http\Controllers;

use DB;
use App\report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function daftarreport(){
        $report = report::where('status', 'Active')
        ->get();
        if ($report) {
            return view('truck_reporting.report_list')->with('datareport', $report);
        }
    }

    public function trashreport(Request $request, $report_id){
        $report = report::find($report_id);
        $report->status = 'Trashed';
        $report->save();
        return redirect('/report');
    }

    public function reportdetail(Request $request, $report_id){
        $report = report::find($report_id);
        if ($report) {
        return view('truck_reporting.report_detail')->with('report', $report);
        }
    }
    
    public function checkindetail(Request $request, $report_id){
        $report = report::find($report_id);
        if ($report) {
            return view('truck_reporting.checkin_detail')->with('report', $report);
        }
    }

    public function checkoutdetail(Request $request, $report_id){
        $report = report::find($report_id);
        if ($report) {
            return view('truck_reporting.checkout_detail')->with('report', $report);
        }
    }

    public function acceptreport(Request $request, $report_id){
        $report = report::find($report_id);
        $report->summary = "Diterima";
        $report->save();
        return view('truck_reporting.report_detail')->with('report', $report);
    }

    public function declinereport(Request $request, $report_id){
        $report = report::find($report_id);
        $report->summary = "Ditolak";
        $report->save();
        return view('truck_reporting.report_detail')->with('report', $report);
    }

    public function addncm(Request $request, $report_id){
        $report = report::find($report_id);
        if ($report) {
            return view('NCM.add_ncm')->with('datareport', $report);
        }
    }

    public function addncmprocess(){
        $report = report::find(request('id'));
        $report->NCM = request('ncm');
        $report->save();
        if ($report) {
            return view('truck_reporting.report_detail')->with('report', $report);
        }
    }
}
