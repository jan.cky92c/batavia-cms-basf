<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transportertypes extends Model
{
    protected $table = 'transporter_types';
    public $primaryKey = 'id';
}
