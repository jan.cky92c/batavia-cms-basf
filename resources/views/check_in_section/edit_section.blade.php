@extends('master')

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="card-title">CHECKIN SECTION - Edit</h5>
    </div>
</div>
<hr/>
<p></p>
<form action="save-update" method="POST" enctype="multipart/form-data">
    @csrf
    <div>
        <label for="desc">Section</label>
        <input type="text" id="desc" name="desc" value="{{$section->description}}" required>
    </div>
    <input type="hidden" name="id" value="{{$section->id}}">
    <hr/>
    <div>
        <button class="btn btn-primary" type="submit">Ubah</button>
    </div>
</form>
@endsection