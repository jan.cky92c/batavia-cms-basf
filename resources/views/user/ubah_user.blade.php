@extends('master')

@section('content')
    <h4>Nomor Induk Karyawan : {{ $user->id_employee }}</h4>
    <p></p>
    <form action="/save-update-user-management" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="users_id" id="users_id" value="{{ $user->users_id}}">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-1">
                <label for="nama">Full Name</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" required id="name" name="name" value="{{$user->name}}" >
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-1">
                <label for="nama">E-mail</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" required id="email" name="email" value="{{$user->email}}" >
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-1">
                <label for="nama">Phone</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" required id="phone" name="phone" value="{{$user->phone}}" >
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-1">
                <label for="status" >Status</label>
            </div>
            <div class="col-md-3">
                <select name="is_active" id="is_active" class="border-left-0 border-top-0 border-right-0 col-5" required>
                    @if ($user->is_active != null)
                    <option value="1" selected>Aktif</option>
                    <option value="0">Tidak Aktif</option>
                    @else
                    <option value="1">Aktif</option>
                    <option value="0" selected>Tidak Aktif</option>
                    @endif
                </select>
            </div>
        </div>
        <div>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-1">
                    <label for="privilege">Previllage</label>
                </div>
                <div class="col-md-3">
                    <select name="id_privilege" id="id_privilege" class="border-left-0 border-top-0 border-right-0 col-5" required>
                        @foreach ($dataPrivileges as $index => $dataPrivilege)
                            <option value="{{$dataPrivilege->id}}">{{$dataPrivilege->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-5 mt-4 mx-auto">
            <button class="btn btn-primary btn-block" type="submit">Save Change</button>
        </div>
    </form>
@endsection