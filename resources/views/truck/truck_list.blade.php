@extends('master')

@section('content')
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>No.</th>
            <th>Truck Name</th>
            <th>Company</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($datatruck as $truck)
        <tr>   
            <td>{{ $i }}</td>
            <td>{{ $truck->truck_name }}</td>
            <td>{{ $truck->company}}</td>
            <td>
                @if($truck->is_active == 0) 
                    <center>
                        <span style="color:white" class="badge bg-danger">Non Active</span>
                    </center>
                @elseif($truck->is_active == 1) 
                    <center>
                        <span style="color:white" class="badge bg-success">Active</span>
                    </center>
                @endif
            </td>
            <td>
                <button class="btn btn-primary"onclick="edit({{$truck->id}})">Ubah</button>
                <button class="btn btn-primary"onclick="deleteTruck({{$truck->id}})">Hapus</button>
            </td>
            @php
                $i = $i + 1;
            @endphp
        </tr>
        @endforeach
    </tbody>
</table>
        
    <a href="/create-truck">
        <button class="btn btn-primary">Tambah Truck</button>
    </a>
@endsection

@section('js')
<script>
function edit(id){
window.location.href = "edit-truck/" +id;
}
function deleteTruck(id){
    var r = confirm("Are you sure to delete this truck ?")
    if(r== true){
        window.location.href = "delete-truck/" +id;
    }
}
</script>    

@endsection