@extends('master')

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="card-title">CHECKOUT SECTION - Create</h5>
    </div>
</div>
<hr/>
<p></p>
<form action="save-create" method="POST" enctype="multipart/form-data">
    @csrf
    <div>
        <label for="desc">Section</label>
        <input type="text" id="desc" name="desc" required>
    </div>
    <hr/>
    <div>
        <button class="btn btn-primary" type="submit">Save</button>
    </div>
</form>
@endsection