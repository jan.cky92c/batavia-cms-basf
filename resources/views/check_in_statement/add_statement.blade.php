@extends('master')

@php
    $datasection = DB::select('select * from check_in_section');
@endphp

@section('content')
<div class="col-md-9">
    <div class="box-header with-border">
        <h5 class="card-title">CHECKIN STATEMENT - Create</h5>
    </div>
</div>
<hr/>
<p></p>
<form action="/save-create" method="POST" enctype="multipart/form-data">
    @csrf
    <div>
        <div class="row">
            <div class="col-md-1">
                <label for="section">Section</label>
            </div>
            <div class="col-md-3">
                <select name="section" id="section">
                    <option selected disabled>-- Chose Section --</option>
                    @foreach ($datasection as $section)
                    <option value="{{$section->id}}">{{ $section->description }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-1">
                <label for="desc">Description</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="desc" name="desc" required>
            </div>
        </div>
    </div>
    <hr/>
    <div>
        <button class="btn btn-primary" type="submit">Create</button>
    </div>
</form>
@endsection