@extends('master')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-md-9">
            <h3 class="card-title">List Approval Trcuks</h3>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-7">
                </div>
                <div class="col-md-5">
                    <div class="btn-group">
                        <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-download"></i>  Export</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<p></p>
<!-- <hr/> -->
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
            <tr>
                <th>No</th>
                <th>Jam Masuk/Keluar</th>
                <th>Nama Angkutan / Pelanggan / Supplier</th>
                <th>No.Kendaraan</th>
                <th>Nama Driver</th>
                <th>Jenis Kendaraan</th>
                <th>Code</th>
                <th style="width: 150px">Kesimpulan</th>
                <th style="width: 150px">Action</th>
            </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach ($approvalTrucks as $truck)
        <tr>   
            <td>{{ $i }}</td>
            <td>{{ $truck->datetime_in_or_out }}</td>
            <td>{{ $truck->carrier_name}}</td>
            <td>{{ $truck->vehicle_number}}</td>
            <td>{{ $truck->name}}</td>
            <td>{{ $truck->vehicle_type}}</td>
            <td>{{ $truck->DO_number}}</td>
            <td>
                Waiting Approval
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-info btn-flat"> <i class="fa fa-download"></i> Actions</button>
                    <button type="button" class="btn btn-info btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span>
                        <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" onclick="edit({{$truck->id}})">Detail</a>
                        <a class="dropdown-item" onClick="approveModal({{$truck->id}})">Approve</a>
                        <a class="dropdown-item" onClick="rejectModal({{$truck->id}})">Reject</a>
                    </button>
                </div>
            </td>
            @php
                $i = $i + 1;
            @endphp
        </tr>
        @endforeach
    </tbody>
</table>
<div class="modal fade" id="modalApproval" tabindex="-1" role="dialog" aria-labelledby="approveLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="approveLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="approve-bodyku">
          <input type="hidden" name="idGeneralInform" id="id_employee">
        </div>
          <div class="modal-footer" id="approve-footerq">
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="rejectLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="rejectLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="reject-bodyku">
          </div>
          <div class="modal-footer" id="reject-footerq">
          </div>
        </div>
      </div>
    </div>
@endsection

@section('js')
<script>

function edit(id){
    var key = encryptKey('{{ env('URL_KEY') }}');
    // window.location.href = "approval-truck/detail/" +encryptData(id.toString(), key);
    window.location.href = "approval-truck/detail/" +id;
}

function confirmdelete(id){
    var r = confirm("Sure, deleted this user?")
    if(r== true){
        window.location.href = "hapus_user/" +id;
    }
}

function approveModal(id){
    localStorage.setItem("idGeneralInform", id);
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Are you sure you want to approve this truck ?</label></div></form>';
    var title = 'Approve Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="approveTruck(id)" type="button" class="btn btn-primary">Yes, approve</button>';
    setModalBoxApprove(title,content,footer,size);
    $('#modalApproval').modal('show');
 }
 function rejectModal(id){
    localStorage.setItem("idGeneralInform", id);
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Add NCM number, if you want to reject this truck ?</label></div> <div class="row"><div class="col-md-1"></div><div class="col-md-2"><label>NCM Number</label></div><div class="col-md-5"><input class="form-control" type="text" id="ncmNumber"><p class="form-helper" style="color:red"><small id="warningNCM" style="color:red; display:none">* Number is required.</small></p></div></div></form>';
    var title = 'Reject Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="rejectTruck()" type="button" class="btn btn-primary">Yes, reject</button>';
    setModalBoxReject(title,content,footer,size);
    $('#modalReject').modal('show');
 }

 function setModalBoxApprove(title,content,footer,$size){
    document.getElementById('approve-bodyku').innerHTML=content;
    document.getElementById('approveLabel').innerText=title;

    document.getElementById('approve-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#approveModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#approveModal').attr('class', 'modal fade')
            .attr('aria-labelledby','approveLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#approveModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

  function setModalBoxReject(title,content,footer,$size){
    document.getElementById('reject-bodyku').innerHTML=content;
    document.getElementById('rejectLabel').innerText=title;

    document.getElementById('reject-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#rejectModal').attr('class', 'modal fade')
            .attr('aria-labelledby','rejectLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

function approveTruck(){
    const id = localStorage.getItem("idGeneralInform");
    localStorage.clear();
    window.location.href = '{{ env('APP_URL') }}' + '/approval-truck/approve/'+id;
}
function rejectTruck(){
    if(document.getElementById('ncmNumber').value===""){
        document.getElementById('warningNCM').style.display="block"
    } else {
        const id = localStorage.getItem("idGeneralInform");
        const key = encryptKey('{{ env('URL_KEY') }}');
        const newData = {
                      id:id,
                      ncm_number:document.getElementById('ncmNumber').value,
                    };
        localStorage.clear();
        // console.log(encryptValue(newData, key))
        const url = '{{ env('APP_URL') }}' + '/approval-truck/rejected'+"?data="+JSON.stringify(newData);
        const xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.send(JSON.stringify(newData));
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                window.location.href = '{{ env('APP_URL') }}' + '/approval-trucks'
            } else{
            }
        };

    }
}

</script>    

@endsection