@extends('master')

@section('content')
<div class="card direct-chat direct-chat-primary">
    <div class="card-header">
        <div class="row">
            <div class="col-md-9">
                <h5 class="card-title">DETAIL - Approval Truck</h5>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-5">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-flat" onClick="approveModal()"> <i class="fa fa-check"></i>  Approve</button>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="btn-group">
                            <button type="button" class="btn btn-danger btn-flat" onClick="rejectModal()"> <i class="fa fa-times"></i>  Reject</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <h4> <i class="far fa-dot-circle"></i> Pemasok (Supplier)</h4>
    </div>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Tanggal & Jam Masuk</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="created_at" name="created_at" value="{{$detailApprovalTruck->created_at}}" >
        </div>
        <div class="col-md-2">
            <label>Jenis SIM</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="license_type" name="license_type" value="{{$detailApprovalTruck->license_type}}">
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Penerima</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="operator_name" name="operator_name" value="{{$detailApprovalTruck->operator_name}}" >
        </div>
        <div class="col-md-2">
            <label>No.SIM</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="license_number" name="license_number" value="{{$detailApprovalTruck->license_number}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Nama Angkutan/ Pelanggan/ Supplier</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="carrier_name" name="carrier_name" value="{{$detailApprovalTruck->carrier_name}}" >
        </div>
        <div class="col-md-2">
            <label>SIM berlaku sampai</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="license_expiry_date" name="license_expiry_date" value="{{$detailApprovalTruck->license_expiry_date}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. Kendaraan</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="vehicle_number" name="vehicle_number" value="{{$detailApprovalTruck->vehicle_number}}" >
        </div>
        <div class="col-md-2">
            <label>STNK berlaku sampai</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="vehicle_certificate_expiry_date" name="vehicle_certificate_expiry_date" value="{{$detailApprovalTruck->vehicle_certificate_expiry_date}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. Tangki</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="tank_number" name="tank_number" value="{{$detailApprovalTruck->tank_number}}" >
        </div>
        <div class="col-md-2">
            <label>KIR berlaku sampai</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="kir_expiry_date" name="kir_expiry_date" value="{{$detailApprovalTruck->kir_expiry_date}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. JO/DO</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="DO_number" name="DO_number" value="{{$detailApprovalTruck->DO_number}}" >
        </div>
        <div class="col-md-2">
            <label>Jenis Kendaraan</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="vehicle_type" name="vehicle_type" value="{{$detailApprovalTruck->vehicle_type}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Nama Driver</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="name" name="name" value="{{$detailApprovalTruck->name}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>No. Telp Driver</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="phone" name="phone" value="{{$detailApprovalTruck->phone}}" >
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            <label>Nama Produk</label>
        </div>
        <div class="col-md-3">
            <input class="form-control" type="text" disabled id="product_name" name="product_name" value="{{$detailApprovalTruck->product_name}}" >
        </div>
    </div>
    <hr/>
    <div class="modal fade" id="modalApproval" tabindex="-1" role="dialog" aria-labelledby="approveLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="approveLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="approve-bodyku">
          </div>
          <div class="modal-footer" id="approve-footerq">
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modalReject" tabindex="-1" role="dialog" aria-labelledby="rejectLabel" aria-hidden="true">
      <div class="modal-dialog ">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="rejectLabel"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          </div>
          <div class="modal-body" id="reject-bodyku">
          </div>
          <div class="modal-footer" id="reject-footerq">
          </div>
        </div>
      </div>
    </div>

</div>
@endsection

@section('js')

<script>
    // this.getId();
function getId() {
    const { pathname } = window.location;
    const arrPathmasterCode = pathname.split('/');
    const id = arrPathmasterCode[arrPathmasterCode.length - 1];
    console.log(id)
  return id;
}

 function approveModal(){
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Are you sure you want to approve this truck ?</label></div> <p class="form-helper"><small class="text-muted danger" id="warningLoanApproved2" style="color:red; display:none">* Total pinjaman melebihi pinjaman yang diinginkan.</small></p></form>';
    var title = 'Approve Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="approveTruck()" type="button" class="btn btn-primary">Yes, approve</button>';
    setModalBoxApprove(title,content,footer,size);
    $('#modalApproval').modal('show');

//   window.location.href = '{{ env('APP_URL') }}' + '/lokasi-kospin/update/'+id;
 }
 function rejectModal(){
    var size="large";
    var content = '<form role="form"><div class="form-group"><label for="exampleInputEmail1">Add NCM number, if you want to reject this truck ?</label></div> <div class="row"><div class="col-md-1"></div><div class="col-md-2"><label>NCM Number</label></div><div class="col-md-5"><input class="form-control" type="text" id="ncmNumber"><p class="form-helper" style="color:red"><small id="warningNCM" style="color:red; display:none">* Number is required.</small></p></div></div></form>';
    var title = 'Reject Truck';
    var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button onclick="rejectTruck()" type="button" class="btn btn-primary">Yes, reject</button>';
    setModalBoxReject(title,content,footer,size);
    $('#modalReject').modal('show');
 }

 function setModalBoxApprove(title,content,footer,$size){
    document.getElementById('approve-bodyku').innerHTML=content;
    document.getElementById('approveLabel').innerText=title;

    document.getElementById('approve-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#approveModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#approveModal').attr('class', 'modal fade')
            .attr('aria-labelledby','approveLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#approveModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

  function setModalBoxReject(title,content,footer,$size){
    document.getElementById('reject-bodyku').innerHTML=content;
    document.getElementById('rejectLabel').innerText=title;

    document.getElementById('reject-footerq').innerHTML=footer;
    if($size == 'large') {
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-lg')
            .attr('aria-labelledby','myLargeModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-lg');
    }
    if($size == 'standart'){
        $('#rejectModal').attr('class', 'modal fade')
            .attr('aria-labelledby','rejectLabel');
        $('.modal-dialog').attr('class','modal-dialog');
    }
    if($size == 'small'){
        $('#rejectModal').attr('class', 'modal fade bs-example-modal-sm')
            .attr('aria-labelledby','mySmallModalLabel');
        $('.modal-dialog').attr('class','modal-dialog modal-sm');
    }
  }

function approveTruck(){
    const id = this.getId();
    window.location.href = '{{ env('APP_URL') }}' + '/approval-truck/approve/'+id;
}

function rejectTruck(){
    if(document.getElementById('ncmNumber').value===""){
        document.getElementById('warningNCM').style.display="block"
    } else {
        const id = this.getId();
        const key = encryptKey('{{ env('URL_KEY') }}');
        const newData = {
                      id:id,
                      ncm_number:document.getElementById('ncmNumber').value,
                    };
        const url = '{{ env('APP_URL') }}' + '/approval-truck/rejected'+"?data="+JSON.stringify(newData);
        const xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.send(JSON.stringify(newData));
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                window.location.href = '{{ env('APP_URL') }}' + '/approval-trucks'
            } else{
            }
        };

    }
}

</script>
@endsection