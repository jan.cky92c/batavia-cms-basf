@extends('master')

@section('content')

<div>
    <h1> Nomor Dokumen : CGK-{{ $report->id }}</h1>
</div>
<h1>Check In Detail</h1>
@php
    $datasection = DB::select('select * from check_in_section');
@endphp
@foreach ($datasection as $section)
    @php
        $dataanswer = DB::table('answer_check_in')
        ->select('answer_check_in.approve_check_in',
                'answer_check_in.decline_check_in',
                'answer_check_in.na_check_in',
                'answer_check_in.annotation_check_in',
                'answer_check_in.section_id',
                'answer_check_in.general_info_id',
                'answer_check_in.master_check_in_id',
                'master_check_in.desc_check_in',
                'master_check_in.id')
        ->leftjoin('master_check_in', 'answer_check_in.master_check_in_id', '=', 'master_check_in.id')
        ->where('answer_check_in.general_info_id', $report->id)
        ->where('answer_check_in.section_id', $section->id)
        ->get();
    @endphp
    <h2>{{ $section->description }}</h2>
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>Description</th>
                <th>Approved</th>
                <th>Declined</th>
                <th>N/A</th>
                <th>Information</th>
            </tr>
        </thead>
        @foreach ($dataanswer as $answer)
        <tbody>
            <tr>
                <td>
                    @if ($answer->desc_check_in != null)
                        {{ $answer->desc_check_in }}
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->approve_check_in != null)
                        Yes
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->decline_check_in != null)
                        Yes
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->na_check_in != null)
                        Yes
                    @else
                        No
                    @endif
                </td>
                <td>
                    @if ($answer->annotation_check_in != null)
                        {{ $answer->annotation_check_in }}
                    @else
                        -
                    @endif
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>
@endforeach
<div class="my-1 mx-auto">
    <a href="/reportdetail/{{$report->id}}">
        <button class="btn btn-primary col-12">Back</button>
    </a>
</div>
@endsection